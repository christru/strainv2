class RemoveStrainRefFromGeographicOrigin < ActiveRecord::Migration
  def change
    remove_reference :geographic_origins, :strain, index: true
  end
end
