class LabTestSerializer < ActiveModel::Serializer
  attributes :id, :TestedBy, :DateTested, :strain_id
  has_many :LabTerps, key: :LabTerps_attributes
  has_many :LabCans, key: :LabCans_attributes
end
