class CreatePerceptionTastes < ActiveRecord::Migration
  def change
    create_table :perception_tastes do |t|
      t.integer :perception_id
      t.integer :taste_id

      t.timestamps
    end
  end
end
