class CreateCannabinoidNames < ActiveRecord::Migration
  def change
    create_table :cannabinoid_names do |t|
      t.string :name
      t.references :cannabinoid, index: true

      t.timestamps
    end
  end
end
