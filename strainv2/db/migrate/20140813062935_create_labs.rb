class CreateLabs < ActiveRecord::Migration
  def change
    create_table :labs do |t|
      t.boolean :tested
      t.references :strain, index: true

      t.timestamps
    end
  end
end
