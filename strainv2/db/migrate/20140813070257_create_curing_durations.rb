class CreateCuringDurations < ActiveRecord::Migration
  def change
    create_table :curing_durations do |t|
      t.integer :duration
      t.references :GrowthCondition, index: true

      t.timestamps
    end
  end
end
