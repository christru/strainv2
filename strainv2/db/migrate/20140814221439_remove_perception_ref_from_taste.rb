class RemovePerceptionRefFromTaste < ActiveRecord::Migration
  def change
    remove_reference :tastes, :perception, index: true
  end
end
