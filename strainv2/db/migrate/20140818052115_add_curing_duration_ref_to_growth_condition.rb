class AddCuringDurationRefToGrowthCondition < ActiveRecord::Migration
  def change
    add_reference :growth_conditions, :CuringDuration, index: true
  end
end
