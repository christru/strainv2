class RemoveGrowthConditionRefFromCuringDuration < ActiveRecord::Migration
  def change
    remove_reference :curing_durations, :GrowthCondition, index: true
  end
end
