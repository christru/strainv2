class CreateAppearances < ActiveRecord::Migration
  def change
    create_table :appearances do |t|
      t.string :appearance
      t.references :perception, index: true

      t.timestamps
    end
  end
end
