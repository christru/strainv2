class RemoveGrowthConditionRefFromStorageContainerType < ActiveRecord::Migration
  def change
    remove_reference :storage_container_types, :GrowthCondition, index: true
  end
end
