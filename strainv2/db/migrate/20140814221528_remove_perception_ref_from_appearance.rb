class RemovePerceptionRefFromAppearance < ActiveRecord::Migration
  def change
    remove_reference :appearances, :perception, index: true
  end
end
