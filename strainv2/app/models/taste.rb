class Taste < ActiveRecord::Base
  has_many :PerceptionTastes
  has_many :perceptions, :through => :PerceptionTastes
end
