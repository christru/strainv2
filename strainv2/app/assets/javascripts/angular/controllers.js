angular.module('app.controllers', ['app.services'])

    //render all strains in a table get only basic information.
    .controller('StrainsViewController', function ($scope, $location, $routeParams, strainService) {
        strainService.getStrains().success(function (strains) {
            $scope.strains = strains;
        }).error(function (error) {
            console.log(error.message);
        });

    })

    //create strain only take in name initially.
    .controller('StrainCreateController', function ($scope, $location, $routeParams, strainService) {
        $scope.form = {};
        var strain = {};

        $scope.create = function () {
            strain.name = angular.copy($scope.form.strain.name);
            strainService.createStrain(strain)
                .success(function (data) {
                    $scope.status = 'Create success!';
                    $location.path("/strains/" + data.id + "/detail/edit");
                })
                .error(function (error) {
                    console.log(error);
                    $location.path('/');
                });
        }

    })

    //should probably think about grouping these services by view instead.
    .controller('DetailEditController', function ($scope, $location, $routeParams, strainService, medicalEffectsService, medicalAilmentsService, appearancesService, smellsService, tastesService, geoService, conTypeService, dryDurationService, cureDurationService) {
            $scope.form = {};
            $scope.form.genetics = {};
            $scope.form.medical = {};
            $scope.form.perception = {};
            $scope.form.selection = {};
            $scope.form.selection.geographic_origin = {};
            $scope.form.selection.geographic_origins = [];

            //collapse sections.
            $scope.form.isStrainCollapsed = false;
            $scope.form.isMedicalCollapsed = true;
            $scope.form.isLabTestsCollapsed = true;
            $scope.form.isPerceptionCollapsed = true;
            $scope.form.isGeneticsCollapsed = true;
            $scope.form.isSubjectiveCollapsed = true;
            $scope.form.isGrowthCollapsed = true;

            //set view only initialy.
            $scope.viewOnly = true;

            //configure multi-select directives
            //medical-section
            //effects
            $scope.form.medical.effectsTagged = [];
            $scope.form.medical.effects = [];
            $scope.form.medical.effectsName = "Effects";

            //ailments
            $scope.form.medical.ailmentsTagged = [];
            $scope.form.medical.ailments = [];
            $scope.form.medical.ailmentsName = "Ailments";

            //perception-section
            //taste
            $scope.form.perception.tasteTagged = [];
            $scope.form.perception.tastes = [];
            $scope.form.perception.tasteName = "Taste";

            //smell
            $scope.form.perception.smellTagged = [];
            $scope.form.perception.smells = [];
            $scope.form.perception.smellName = "Smell";

            //appearance
            $scope.form.perception.appearanceTagged = [];
            $scope.form.perception.appearances = [];
            $scope.form.perception.appearanceName = "Appearance";

            populateDropDown();
            getStrainData();

        //unlock form for editing.
        $scope.edit = function () {
            $scope.viewOnly = false;
        };

        //resets form view
        $scope.cancel = function () {
            $scope.viewOnly = true;
            $scope.form = angular.copy($scope.formCopy);
            $scope.strain = angular.copy($scope.strainCopy);
        };

        //save strain
        $scope.save = function () {
            addMultiSelectInfo();
            //only want id from parent id element.
            $scope.strain.parent_id = $scope.strain.parent_id.id;
            strainService.updateStrain($scope.strain)
                .success(function () {
                    $location.path('/');
                })
                .error(function (error) {
                    console.log(error);
                })
        };

        //get strain data
        function getStrainData() {
            strainService.getStrain($routeParams.id)
                .success(function (data) {
                    $scope.strain = data;
                    if ($scope.strain) {
                        doSomeStuffAfterPromise();
                    }
                })
                .error(function (error) {
                    console.log(error);
                })
        }

        function doSomeStuffAfterPromise() {
            //will refactor this later.

            //set multi-select info for medical effect
            if ($scope.strain.medical_attributes && $scope.strain.medical_attributes.MedicalEffect_ids) {
                for (var i = 0; i < $scope.strain.medical_attributes.MedicalEffect_ids.length; i++) {
                    $scope.form.medical.effectsTagged.push({
                        "id": $scope.strain.medical_attributes.MedicalEffect_ids[i].id,
                        "name": $scope.strain.medical_attributes.MedicalEffect_ids[i].effect,
                        "hide": "true"
                    })
                }
                for (var i = 0; i < $scope.strain.medical_attributes.MedicalEffect_ids.length; i++) {
                    for (var j = 0; j < $scope.form.medical.effects.length; j++) {
                        if ($scope.strain.medical_attributes.MedicalEffect_ids[i].id == $scope.form.medical.effects[j].id) {
                            $scope.form.medical.effects[j].hide = true;
                            break;
                        }
                    }
                }
            }

            //set multi-select info for medical ailment
            if ($scope.strain.medical_attributes && $scope.strain.medical_attributes.MedicalAilment_ids) {
                for (var i = 0; i < $scope.strain.medical_attributes.MedicalAilment_ids.length; i++) {
                    $scope.form.medical.ailmentsTagged.push({
                        "id": $scope.strain.medical_attributes.MedicalAilment_ids[i].id,
                        "name": $scope.strain.medical_attributes.MedicalAilment_ids[i].ailment,
                        "hide": "true"
                    })
                }
                for (var i = 0; i < $scope.strain.medical_attributes.MedicalAilment_ids.length; i++) {
                    for (var j = 0; j < $scope.form.medical.ailments.length; j++) {
                        if ($scope.strain.medical_attributes.MedicalAilment_ids[i].id == $scope.form.medical.ailments[j].id) {
                            $scope.form.medical.ailments[j].hide = true;
                            break;
                        }
                    }
                }
            }

            //set multi-select info for perception taste
            if ($scope.strain.perception_attributes && $scope.strain.perception_attributes.taste_ids) {
                for (var i = 0; i < $scope.strain.perception_attributes.taste_ids.length; i++) {
                    $scope.form.perception.tasteTagged.push({
                        "id": $scope.strain.perception_attributes.taste_ids[i].id,
                        "name": $scope.strain.perception_attributes.taste_ids[i].taste,
                        "hide": "true"
                    })
                }
                for (var i = 0; i < $scope.strain.perception_attributes.taste_ids.length; i++) {
                    for (var j = 0; j < $scope.form.perception.tastes.length; j++) {
                        if ($scope.strain.perception_attributes.taste_ids[i].id == $scope.form.perception.tastes[j].id) {
                            $scope.form.perception.tastes[j].hide = true;
                            break;
                        }
                    }
                }
            }

            //set multi-select info for perception smell
            if ($scope.strain.perception_attributes && $scope.strain.perception_attributes.smell_ids) {
                for (var i = 0; i < $scope.strain.perception_attributes.smell_ids.length; i++) {
                    $scope.form.perception.smellTagged.push({
                        "id": $scope.strain.perception_attributes.smell_ids[i].id,
                        "name": $scope.strain.perception_attributes.smell_ids[i].smell,
                        "hide": "true"
                    })
                }
                for (var i = 0; i < $scope.strain.perception_attributes.smell_ids.length; i++) {
                    for (var j = 0; j < $scope.form.perception.smells.length; j++) {
                        if ($scope.strain.perception_attributes.smell_ids[i].id == $scope.form.perception.smells[j].id) {
                            $scope.form.perception.smells[j].hide = true;
                            break;
                        }
                    }
                }
            }
            //set multi-select info for perception appearance
            if ($scope.strain.perception_attributes && $scope.strain.perception_attributes.appearance_ids) {
                for (var i = 0; i < $scope.strain.perception_attributes.appearance_ids.length; i++) {
                    $scope.form.perception.appearanceTagged.push({
                        "id": $scope.strain.perception_attributes.appearance_ids[i].id,
                        "name": $scope.strain.perception_attributes.appearance_ids[i].appearance,
                        "hide": "true"
                    })
                }
                for (var i = 0; i < $scope.strain.perception_attributes.appearance_ids.length; i++) {
                    for (var j = 0; j < $scope.form.perception.appearances.length; j++) {
                        if ($scope.strain.perception_attributes.appearance_ids[i].id == $scope.form.perception.appearances[j].id) {
                            $scope.form.perception.appearances[j].hide = true;
                            break;
                        }
                    }
                }
            }

            //leave this at the end used as a clean copy to revert back if user hit cancel.
            $scope.formCopy = angular.copy($scope.form);
            $scope.strainCopy = angular.copy($scope.strain);
        }

        function addMultiSelectInfo() {

            if (!$scope.strain.medical_attributes) {
                $scope.strain.medical_attributes = {};
            }
            if (!$scope.strain.perception_attributes) {
                $scope.strain.perception_attributes = {};
            }
            $scope.strain.medical_attributes.MedicalEffect_ids = [];
            for (var i = 0; i < $scope.form.medical.effectsTagged.length; i++) {
                $scope.strain.medical_attributes.MedicalEffect_ids.push($scope.form.medical.effectsTagged[i].id);
            }
            //get medical ailment tags.
            $scope.strain.medical_attributes.MedicalAilment_ids = [];
            for (var i = 0; i < $scope.form.medical.ailmentsTagged.length; i++) {
                $scope.strain.medical_attributes.MedicalAilment_ids.push($scope.form.medical.ailmentsTagged[i].id);
            }
            //get perception taste tags
            $scope.strain.perception_attributes.taste_ids = [];
            for (var i = 0; i < $scope.form.perception.tasteTagged.length; i++) {
                $scope.strain.perception_attributes.taste_ids.push($scope.form.perception.tasteTagged[i].id);
            }
            //get perception smell tags
            $scope.strain.perception_attributes.smell_ids = [];
            for (var i = 0; i < $scope.form.perception.smellTagged.length; i++) {
                $scope.strain.perception_attributes.smell_ids.push($scope.form.perception.smellTagged[i].id);
            }
            //get perception appearance tags
            $scope.strain.perception_attributes.appearance_ids = [];
            for (var i = 0; i < $scope.form.perception.appearanceTagged.length; i++) {
                $scope.strain.perception_attributes.appearance_ids.push($scope.form.perception.appearanceTagged[i].id);
            }
        }

        function populateDropDown() {
            //get list of medical effects
            medicalEffectsService.getMedicalEffects()
                .success(function (data) {
                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.medical.effects.push({
                            id: data[i].id,
                            name: data[i].effect,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of container types
            conTypeService.getConTypes()
                .success(function (data) {
                    $scope.form.selection.storage_container_types = data;
                });

            //get list of dry durations
            dryDurationService.getDryDuration()
                .success(function (data) {
                    $scope.form.selection.drying_durations = data;
                });

            //get list of cure durations
            cureDurationService.getCureDuration()
                .success(function (data) {
                    $scope.form.selection.curing_durations = data;
                });

            //get list of ailements
            medicalAilmentsService.getMedicalAilments()
                .success(function (data) {
                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.medical.ailments.push({
                            id: data[i].id,
                            name: data[i].ailment,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of tastes
            tastesService.getTastes()
                .success(function (data) {
                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.perception.tastes.push({
                            id: data[i].id,
                            name: data[i].taste,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of smells
            smellsService.getSmells()
                .success(function (data) {
                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.perception.smells.push({
                            id: data[i].id,
                            name: data[i].smell,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of appearances
            appearancesService.getAppearances()
                .success(function (data) {
                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.perception.appearances.push({
                            id: data[i].id,
                            name: data[i].appearance,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of parents
            strainService.getStrains()
                .success(function (data) {
                    $scope.form.genetics.parents = angular.copy(data);
                })
                .error(function (error) {
                    console.log(error);
                });

            //get list of geography origins
            geoService.getGeoOrigins()
                .success(function (data) {

                    for (var i = data.length - 1; i >= 0; i--) {
                        $scope.form.selection.geographic_origins.push({
                            id: data[i].id,
                            name: data[i].location,
                            hide: false
                        })
                    }
                })
                .error(function (error) {
                    console.log(error);
                });
        }})

    //view many tests
    .controller('TestsViewController', function ($scope, $location, $routeParams, labTestService) {
        $scope.strain = {};
        $scope.strain.id = $routeParams.id;

        labTestService.getTests($routeParams.id)
            .success(function (data) {
                $scope.tests = data;

            })
    })

    //create a test
    .controller('TestCreateController', function ($scope, $location, $routeParams, labTestService) {

        $scope.save = function () {

            labTestService.createTest($routeParams.id, $scope.test)
                .success(function (data) {
                    $location.path("/strains/" + $routeParams.id + "/tests/" + data.id + "/edit");

                }).
                error(function (error) {
                    //console.log(error);
                })
        };
    })
    //view or edit test
    .controller('TestEditController', function ($scope, $location, $routeParams, labTestService, terpService, canService) {
        $scope.test = {};
        var addType;
        $scope.sampleForm = {};
        $scope.terpTableList = [];
        $scope.canTableList = [];
        $scope.sampleSelection = [];
        $scope.viewOnly = true;

        //unlock form for editing.
        $scope.edit = function () {
        $scope.viewOnly = false;
        };


        //get drop down info
        terpService.getTerpSelections().success(function (data) {
            $scope.terps = data;
        });

        //get drop down info
        canService.getCanSelections().success(function (data) {
            $scope.cans = data;
        });

        //get test
        labTestService.getTest($routeParams.strainID, $routeParams.id)
            .success(function (data) {
                $scope.test = data;
                $scope.testCopy = angularCopy($scope.test);
            });

        //rest form back to clean slate.
        $scope.cancel = function () {
            $scope.test = $scope.testCopy;
            $scope.viewOnly = true;
        };

        //save updated test.
        $scope.save = function () {
            labTestService.updateTest($routeParams.strainID, $routeParams.id, $scope.test)
                .success(function () {
                    $scope.testCopy = angularCopy($scope.test);
                    $scope.viewOnly = true;
                }).
                error(function (error) {
                    $scope.test = $scope.testCopy;
                    $scope.viewOnly = true;
                    console.log(error);
                })

        };

        //create terpenoid
        $scope.createTerp = function () {
            //set drop down list.
            $scope.sampleSelection = angular.copy($scope.terps);
            //set type to add.
            addType = "terp";
            //show sample form.
            $scope.showSample = true;
        };

        $scope.addSample = function () {
            //add sample
            if (addType == "terp") {
                var sampleObject = {};
                sampleObject.terpenoid = {};
                sampleObject.terpenoid_id = angular.copy($scope.sampleForm.id);
                sampleObject.concentration = angular.copy($scope.sampleForm.concentration);

                terpService.createLabTerp($routeParams.strainID, $routeParams.id, sampleObject)
                    .success(function () {
                    //on success find value/name of the id and push to list.
                    for (var i = 0; i < $scope.sampleSelection.length; i++) {
                        if (sampleObject.terpenoid_id == $scope.sampleSelection[i].id) {
                            sampleObject.terpenoid.name = $scope.sampleSelection[i].name;
                            break;
                        }
                    }
                    $scope.test.LabTerps_attributes.push(sampleObject);
                    //reset form
                    $scope.sampleForm = {};
                    sampleObject = {};
                    addType = null;
                    //hide form
                    $scope.showSample = false;
                });

            }
            else if (addType == "can") {
                sampleObject = {};
                sampleObject.cannabinoid = {};
                sampleObject.cannabinoid_id = angular.copy($scope.sampleForm.id);
                sampleObject.concentration = angular.copy($scope.sampleForm.concentration);

                canService.createLabCan($routeParams.strainID, $routeParams.id, sampleObject)
                    .success(function () {
                        for (var i = 0; i < $scope.sampleSelection.length; i++) {
                            if (sampleObject.cannabinoid_id == $scope.sampleSelection[i].id) {
                                sampleObject.cannabinoid.name = $scope.sampleSelection[i].name;
                                break;
                            }
                        }
                        $scope.test.LabCans_attributes.push(sampleObject);
                        //reset form
                        $scope.sampleForm = {};
                        sampleObject = {};
                        addType = null;
                        //hide form
                        $scope.showSample = false;
                    });
            }
            else {
                //reset form
                addType = null;
                $scope.sampleForm = {};
                //hide form
                $scope.showSample = false;
            }

        };

        //cancel sample creation
        $scope.cancelSample = function () {
            //clear sample data
            $scope.sampleForm = {};
            //hide form
            $scope.showSample = false;
        };

        //create cannabinoid
        $scope.createCan = function () {
            $scope.sampleSelection = angular.copy($scope.cans);
            addType = "can";
            $scope.showSample = true;
        }
    });
