class GrowthCondition < ActiveRecord::Base
  belongs_to :strain
  belongs_to :StorageContainerType
  belongs_to :drying_duration
  belongs_to :curing_duration
end
