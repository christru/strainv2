angular.module('app.directives', [])

    .directive("multiSelectItems", function () {
        //expecting format {id,name} using bootstrap 3 select

        var template = '<div class="form-group"><label class="col-md-4 control-label" for="{{name}}">{{name}}</label><div class="col-md-4"><select id="{{name}}" ng-model="currentlySelected" name="{{name}}" class="form-control"><option ng-repeat="t in items | filter:{ hide:false }" value="{{t.id}}">{{t.name}}</option></select></div><div class="col-md-4"><button id="{{name}}button" ng-click="add()" name="{{name}}button" class="btn btn-primary">Add</button></div><div class="col-md-4 col-md-offset-4"><div><button  ng-repeat="d in tagged" id="{{d.id}}" ng-click="destroy($index,d.id)" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> {{d.name }}</button></div></div></div></div>';

        return {
            scope: {
                items: "=listItems",
                tagged: "=taggedItems",
                name: "="
             },

            controller: function ($scope) {
                $scope.currentlySelected = null;

                $scope.destroy = function (i, id) {
                    $scope.tagged.splice(i, 1);
                    for (var i = 0; i < $scope.items.length; i++) {
                            if ($scope.items[i].id == id) {
                                $scope.items[i].hide = false;
                                break;
                            }
                        }
                };

                $scope.add = function () {
                    if ($scope.currentlySelected != null) {
                        for (var i = 0; i < $scope.items.length; i++) {
                            if ($scope.items[i].id == $scope.currentlySelected) {
                                //prevent double click entry set selected to null first.
                                $scope.currentlySelected = null;
                                $scope.tagged.push($scope.items[i]);
                                $scope.items[i].hide = true;
                                break;
                            }
                        }
                    }
                }
            },
            restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
            template: template,
            replace: false
        };
    }
);
