class RemoveLabTestRefFromLabTerp < ActiveRecord::Migration
  def change
    remove_reference :lab_terps, :LabTest, index: true
  end
end
