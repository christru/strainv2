class Terpenoid < ActiveRecord::Base
  has_many :LabTerps
  has_many :LabTests, :through => :LabTerps
end
