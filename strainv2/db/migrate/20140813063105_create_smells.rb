class CreateSmells < ActiveRecord::Migration
  def change
    create_table :smells do |t|
      t.string :smell
      t.references :perception, index: true

      t.timestamps
    end
  end
end
