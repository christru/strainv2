class CreateTerpenoidAkas < ActiveRecord::Migration
  def change
    create_table :terpenoid_akas do |t|
      t.string :aka
      t.references :terpenoid, index: true

      t.timestamps
    end
  end
end
