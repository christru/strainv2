class CreateTerpenoidSynergies < ActiveRecord::Migration
  def change
    create_table :terpenoid_synergies do |t|
      t.string :synergy
      t.references :terpenoid, index: true

      t.timestamps
    end
  end
end
