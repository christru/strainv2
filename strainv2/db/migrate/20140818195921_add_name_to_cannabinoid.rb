class AddNameToCannabinoid < ActiveRecord::Migration
  def change
    add_column :cannabinoids, :name, :string
  end
end
