class CreateStrainAkas < ActiveRecord::Migration
  def change
    create_table :strain_akas do |t|
      t.string :name
      t.references :strain, index: true

      t.timestamps
    end
  end
end
