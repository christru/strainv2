class Strain < ActiveRecord::Base
  has_many :strain_akas, dependent: :destroy
  has_many :lab_tests, dependent: :destroy
  has_one :medical, dependent: :destroy
  has_one :lab, dependent: :destroy
  has_one :perception, dependent: :destroy
  has_one :growth_condition, dependent: :destroy
  has_one :subjective, dependent: :destroy
  has_one :genetic, dependent: :destroy

  has_many :children, class_name: "Strain", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Strain"

  accepts_nested_attributes_for :strain_akas
  accepts_nested_attributes_for :lab_tests
  accepts_nested_attributes_for :medical
  accepts_nested_attributes_for :lab
  accepts_nested_attributes_for :perception
  accepts_nested_attributes_for :growth_condition
  accepts_nested_attributes_for :subjective
  accepts_nested_attributes_for :genetic
end
