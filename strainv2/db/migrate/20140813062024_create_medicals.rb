class CreateMedicals < ActiveRecord::Migration
  def change
    create_table :medicals do |t|
      t.references :strain, index: true

      t.timestamps
    end
  end
end
