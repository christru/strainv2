class Perception < ActiveRecord::Base
  belongs_to :strain
  has_and_belongs_to_many :smells
  has_and_belongs_to_many :appearances

  has_many :PerceptionTastes
  has_many :tastes, :through => :PerceptionTastes

  #this creates taste.
  accepts_nested_attributes_for :tastes
  accepts_nested_attributes_for :PerceptionTastes

end
