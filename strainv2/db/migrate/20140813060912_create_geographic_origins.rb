class CreateGeographicOrigins < ActiveRecord::Migration
  def change
    create_table :geographic_origins do |t|
      t.string :location
      t.references :strain, index: true

      t.timestamps
    end
  end
end
