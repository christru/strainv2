class CreateLabCans < ActiveRecord::Migration
  def change
    create_table :lab_cans do |t|
      t.integer :LabTest_id
      t.integer :cannabinoid_id
      t.float :concentration

      t.timestamps
    end
  end
end
