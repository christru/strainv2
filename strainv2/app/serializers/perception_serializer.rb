class PerceptionSerializer < ActiveModel::Serializer
  attributes :id
  has_many :smells, key: :smell_ids
  has_many :tastes, key: :taste_ids
  has_many :appearances, key: :appearance_ids
end
