class LabTest < ActiveRecord::Base
  belongs_to :strain
  has_many :LabCans
  has_many :cannabinoids, :through => :LabCans
  has_many :LabTerps
  has_many :terpenoids, :through => :LabTerps

  accepts_nested_attributes_for :LabTerps
  accepts_nested_attributes_for :LabCans
end
