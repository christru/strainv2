class Api::TerpenoidsController < ApplicationController
  respond_to :json

  def index
    render :json => Terpenoid.all.to_json(:only => [:id,:name])
  end

end
