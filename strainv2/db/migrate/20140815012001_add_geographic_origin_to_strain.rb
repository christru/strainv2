class AddGeographicOriginToStrain < ActiveRecord::Migration
  def change
    add_column :strains, :GeographicOrigin, :string
  end
end
