class Api::StrainsController < ApplicationController
  respond_to :json
  before_action :set_strain, only: [:show, :edit, :update, :destroy]
  wrap_parameters :strain, include: [
    :id,
    :name,
    :parent_id,
    :GeographicOrigin,
    :strain_akas_attributes,
    :geographic_origin_attributes,
    :lab_tests_attributes,
    :medical_attributes,
    :lab_attributes,
    :perception_attributes,
    :growth_condition_attributes,
    :subjective_attributes,
    :genetic_attributes,
    :tastes
  ]
  #rendering this way as to avoid AMS defaults and additional querying.
  def index
    render :json => Strain.all.to_json(:only => [:id,:name])
  end

  def create
    respond_with :api, Strain.create(strain_params)
  end

  #this will use AMS and will retrieve all details excluding tests for strain.
  def show
    respond_with(@strain)
  end

  def update
    #https://github.com/rails/rails/issues/8832
    params[:strain][:medical_attributes][:MedicalEffect_ids] ||= []
    params[:strain][:medical_attributes][:MedicalAilment_ids] ||= []
    params[:strain][:perception_attributes][:smell_ids] ||= []
    params[:strain][:perception_attributes][:taste_ids] ||= []
    params[:strain][:perception_attributes][:appearance_ids] ||= []
    respond_with :api, @strain.update(strain_params)
  end

  private

  def set_strain
    @strain = Strain.find(params[:id])
  end

  def strain_params
    params.require(:strain).permit(
      :id,
      :name,
      :parent_id, #this does not work as of now. record works and reads fine but cant post to it.
      :GeographicOrigin,
      strain_akas_attributes:[:id,:name],
      lab_tests_attributes:[:id, :TestedBy, :DateTested],
      medical_attributes:[:id, MedicalEffect_ids:[],MedicalAilment_ids:[]],
      lab_attributes:[:id, :tested],
      perception_attributes:[:id, taste_ids:[], smell_ids:[], appearance_ids:[]],
      growth_condition_attributes:[:id, :StorageContainerType_id, :CuringDuration_id, :DryingDuration_id, :LightIntensity, :BulbType, :BulbPower, :GrowingMedium, :VegetativeLight, :VegetativeDuration, :NutrientCompositionVeg, :FlowerLight, :FlowerDuration, :NutrientCompositionFlower, :DryingTemperature, :DryingRelativeHumidity, :StorageTemperature, :StorageDuration, :breeder, :yield],
      subjective_attributes:[:id, :HowHigh, :news, :controversy, :TheScoop, :available, :concentrate, :flower, :TheVerdict, :lined, :pairings, :reviews, :PopCulture],
      genetic_attributes:[:id, :SativaPercentage, :IndicaPercentage]
      )
end
end
