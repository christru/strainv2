class RemoveLabTestRefFromTerpenoid < ActiveRecord::Migration
  def change
    remove_reference :terpenoids, :LabTest, index: true
  end
end
