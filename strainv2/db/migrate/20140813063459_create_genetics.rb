class CreateGenetics < ActiveRecord::Migration
  def change
    create_table :genetics do |t|
      t.decimal :SativaPercentage
      t.decimal :IndicaPercentage
      t.references :strain, index: true

      t.timestamps
    end
  end
end
