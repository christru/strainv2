class StrainSerializer < ActiveModel::Serializer
  attributes :id, :name, :GeographicOrigin
  attribute :parent, key: :parent_id
  has_many :strain_akas, key: :strain_akas_attributes
  has_many :lab_tests, key: :lab_tests_attributes
  has_one :medical, key: :medical_attributes
  has_one :lab, key: :lab_attributes
  has_one :perception, key: :perception_attributes
  has_one :growth_condition, key: :growth_condition_attributes
  has_one :subjective, key: :subjective_attributes
  has_one :genetic, key: :genetic_attributes
end
