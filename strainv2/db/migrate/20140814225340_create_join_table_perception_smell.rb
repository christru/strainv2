class CreateJoinTablePerceptionSmell < ActiveRecord::Migration
  def change
    create_join_table :perceptions, :smells do |t|
      # t.index [:perception_id, :smell_id]
      # t.index [:smell_id, :perception_id]
    end
  end
end
