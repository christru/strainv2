class CreatePerceptions < ActiveRecord::Migration
  def change
    create_table :perceptions do |t|
      t.references :strain, index: true

      t.timestamps
    end
  end
end
