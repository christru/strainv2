class AddParentRefToStrain < ActiveRecord::Migration
  def change
    add_reference :strains, :parent, index: true
  end
end
