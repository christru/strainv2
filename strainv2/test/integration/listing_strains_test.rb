require 'test_helper'

class ListingStrainsTest < ActionDispatch::IntegrationTest
  setup do
    #! raises warning if doesnt create.
    Strain.create!(name: 'girlscout')
    Strain.create!(name: 'mexican mersh')
  end

  test 'list strains' do
    get '/api/strains'

    assert_equal 200, response.status
    assert_equal Mime::JSON, response.content_type
    assert_equal Strain.count, JSON.parse(response.body).size
  end
end
