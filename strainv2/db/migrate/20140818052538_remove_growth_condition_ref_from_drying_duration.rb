class RemoveGrowthConditionRefFromDryingDuration < ActiveRecord::Migration
  def change
    remove_reference :drying_durations, :GrowthCondition, index: true
  end
end
