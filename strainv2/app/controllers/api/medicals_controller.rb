class Api::MedicalsController < ApplicationController
  before_action :set_strain, only: [:show, :edit, :update, :destroy]
  respond_to :json

  def show
    respond_with @strain.medical
  end


  private
  def set_strain
    @strain = Strain.find(params[:strain_id])
  end
end
