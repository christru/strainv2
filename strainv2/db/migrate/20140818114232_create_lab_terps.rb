class CreateLabTerps < ActiveRecord::Migration
  def change
    create_table :lab_terps do |t|
      t.integer :LabTest_id
      t.integer :terpenoid_id
      t.float :concentration

      t.timestamps
    end
  end
end
