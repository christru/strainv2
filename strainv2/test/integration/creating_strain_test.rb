require 'test_helper'

class CreatingStrainTest < ActionDispatch::IntegrationTest

  test "Create strain" do
    post '/api/strains', { strain: strain_attributes }.to_json, {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }


    assert_equal 201, response.status
    strain = json(response.body)
    assert_equal api_strain_url(strain[:id]), response.location
    assert_equal strain_attributes[:name], strain[:name]
    assert_equal strain_attributes[:lab_tests_attributes], strain[:lab_tests]
  end


  def strain_attributes
    {
      name: 'chris gone crazy',
      lab_attributes:{tested:'false'},
      lab_tests_attributes:[{TestedBy:'somedude',DateTested:'12-2-1982'}],

    }
  end

end





