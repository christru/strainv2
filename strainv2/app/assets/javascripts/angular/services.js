angular.module('app.services', [])

    .service('strainService', ['$http', function ($http) {

        var url = '/api/strains';

        this.getStrains = function () {
            return $http.get(url);
        };

        this.getStrain = function (id) {
            return $http.get(url + '/' + id);
        };

        this.createStrain = function (strn) {
            return $http.post(url, strn);
        };

        this.updateStrain = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteStrain = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])

    .service('labTestService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'lab_tests';

        this.getTests = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };

        this.getTest = function (strainid, id) {
            return $http.get(url + '/' + strainid + '/' + endpoint + "/" + id);
        };

        this.createTest = function (id, test) {
            return $http.post(url + '/' + id + '/' + endpoint, test);
        };

        this.updateTest = function (strainid, id, test) {
            return $http.put(url + '/' + strainid + '/' + endpoint + "/" + id, test)
        };

        this.deleteStrain = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])

     .service('terpService', ['$http', function ($http) {

        var url = '/api/strains/';
        var endpoint = '/lab_terps';

        this.getTerpSelections = function () {
            return $http.get('/api/selections/' + 'terpenoids');
        };

        this.createLabTerp = function (id, testid,labterp) {
            return $http.post(url + id  + '/lab_tests/' + testid + endpoint, labterp);
        };


    }])

     .service('canService', ['$http', function ($http) {

        var url = '/api/strains/';
        var endpoint = '/lab_cans';

        this.getCanSelections = function () {
            return $http.get('/api/selections/' + 'cannabinoids');
        };

        this.createLabCan = function (id, testid, labcan) {
            return $http.post(url + id  + '/lab_tests/' + testid + endpoint, labcan);
        };

    }])


     .service('medicalEffectsService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'medical_effects';

        this.getMedicalEffects = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('medicalAilmentsService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'medical_ailments';

        this.getMedicalAilments = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('tastesService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'tastes';

        this.getTastes = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('smellsService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'smells';

        this.getSmells = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('appearancesService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'appearances';

        this.getAppearances = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('geoService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'geographic_origins';

        this.getGeoOrigins = function () {
            return $http.get(url + endpoint);
        };

    }])

    //cure duration
    .service('cureDurationService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'curing_durations';

        this.getCureDuration = function () {
            return $http.get(url + endpoint);
        };

    }])
    //dry duration
    .service('dryDurationService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'drying_durations';

        this.getDryDuration = function () {
            return $http.get(url + endpoint);
        };

    }])
    //container type
    .service('conTypeService', ['$http', function ($http) {

        var url = '/api/selections/';
        var endpoint = 'storage_container_types';

        this.getConTypes = function () {
            return $http.get(url + endpoint);
        };

    }])

    .service('geneticService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'genetic';

        this.getGenetic = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])


    .service('growthConditionService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'growth_condition';

        this.getGrowthCondition = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])

    .service('labService', ['$http', function ($http) {


        var url = '/api/strains';
        var endpoint = 'lab';

        this.getLab = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])

    .service('medicalService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'medical';

        this.getMedical = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])

    .service('perceptionService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'perception';

        this.getPerception = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }])


    .service('subjectiveService', ['$http', function ($http) {

        var url = '/api/strains';
        var endpoint = 'subjective';

        this.getSubjective = function (id) {
            return $http.get(url + '/' + id + '/' + endpoint);
        };


        this.createGenetic = function (strn) {
            return $http.post(url, strn);
        };

        this.updateGenetic = function (strn) {
            return $http.put(url + '/' + strn.id, strn)
        };

        this.deleteGenetic = function (id) {
            return $http.delete(url + '/' + id);
        };

    }]);



