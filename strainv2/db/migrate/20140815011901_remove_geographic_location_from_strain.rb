class RemoveGeographicLocationFromStrain < ActiveRecord::Migration
  def change
    remove_column :strains, :GeographicLocation, :string
  end
end
