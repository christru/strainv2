class LabCanSerializer < ActiveModel::Serializer
  attributes :id, :cannabinoid_id, :concentration
  has_one :cannabinoid
end
