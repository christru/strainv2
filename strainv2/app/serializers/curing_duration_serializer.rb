class CuringDurationSerializer < ActiveModel::Serializer
  attributes :id, :duration
end
