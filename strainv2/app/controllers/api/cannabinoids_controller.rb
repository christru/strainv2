class Api::CannabinoidsController < ApplicationController
 respond_to :json

 def index
  render :json => Cannabinoid.all.to_json(:only => [:id,:name])
end

end

