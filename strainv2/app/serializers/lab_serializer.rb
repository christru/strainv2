class LabSerializer < ActiveModel::Serializer
  attributes :id, :tested, :strain_id
end
