class AddStorageContainerTypeRefToGrowthCondition < ActiveRecord::Migration
  def change
    add_reference :growth_conditions, :StorageContainerType, index: true
  end
end
