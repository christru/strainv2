class DryingDurationSerializer < ActiveModel::Serializer
  attributes :id, :duration
end
