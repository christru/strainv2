class Api::LabTerpsController < ApplicationController
  before_action :set_strain, :set_test
  respond_to :json

  def create
    @labterp = @test.LabTerps.create(lab_test_params)
    respond_with(:api,@strain,@test,@labterp)
  end

  def update
    @labterp = @test.LabTerps.update(params[:id], lab_test_params)
    respond_with @labterp
  end

  private
  def set_strain
    @strain = Strain.find(params[:strain_id])
  end

  def set_test
    @test = LabTest.find(params[:lab_test_id])
  end

  def lab_test_params
    params.permit(:lab_test, :id, :TestedBy, :DateTested, :terpenoid_id, :lab_test_id, :concentration)
  end

end
