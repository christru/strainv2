class CreateMedicalAilments < ActiveRecord::Migration
  def change
    create_table :medical_ailments do |t|
      t.string :ailment
      t.references :medical, index: true

      t.timestamps
    end
  end
end
