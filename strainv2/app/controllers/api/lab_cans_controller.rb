class Api::LabCansController < ApplicationController
  before_action :set_strain, :set_test
  respond_to :json

  def create
    @labcan = @test.LabCans.create(lab_test_params)
    respond_with(:api,@strain,@test,@labcan)
  end

  def update
    @labcan = @test.LabCans.update(params[:id], lab_test_params)
    respond_with @labcan
  end

  private
  def set_strain
    @strain = Strain.find(params[:strain_id])
  end

  def set_test
    @test = LabTest.find(params[:lab_test_id])
  end

  def lab_test_params
    params.permit(:lab_test, :id, :TestedBy, :DateTested, :cannabinoid_id, :lab_test_id, :concentration)
  end

end
