class CreateJoinTablePerceptionAppearance < ActiveRecord::Migration
  def change
    create_join_table :perceptions, :appearances do |t|
      # t.index [:perception_id, :appearance_id]
      # t.index [:appearance_id, :perception_id]
    end
  end
end
