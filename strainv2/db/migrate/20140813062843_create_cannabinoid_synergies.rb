class CreateCannabinoidSynergies < ActiveRecord::Migration
  def change
    create_table :cannabinoid_synergies do |t|
      t.string :synergy
      t.references :cannabinoid, index: true

      t.timestamps
    end
  end
end
