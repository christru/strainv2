class Api::LabTestsController < ApplicationController
  before_action :set_strain
  respond_to :json

  def show
    respond_with @strain.lab_tests.find(params[:id])
  end

  def index
    respond_with @strain.lab_tests
  end

  def create
    @labtest = @strain.lab_tests.create(lab_test_params)
    respond_with(:api, @strain, @labtest)
  end

  def update
    @labtest = @strain.lab_tests.update(params[:id], lab_test_params)
    respond_with @labtest
  end

  private
  def set_strain
    @strain = Strain.find(params[:strain_id])
  end

  def lab_test_params
    params.permit(:lab_test, :strain_id, :id, :TestedBy, :DateTested, LabTerps_attributes:[:terpenoid_id,:lab_test_id, :concentration],LabCans_attributes:[:cannabinoid_id,:lab_test_id, :concentration])
  end

end
