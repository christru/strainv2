class CreateTerpenoidNames < ActiveRecord::Migration
  def change
    create_table :terpenoid_names do |t|
      t.string :name
      t.references :terpenoid, index: true

      t.timestamps
    end
  end
end
