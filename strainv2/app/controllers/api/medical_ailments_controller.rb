class Api::MedicalAilmentsController < ApplicationController
  before_action :set_strain, only: [:show, :edit, :update, :destroy]
  respond_to :json

  def index
    respond_with MedicalAilment.all
  end

end
