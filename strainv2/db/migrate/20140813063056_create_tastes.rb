class CreateTastes < ActiveRecord::Migration
  def change
    create_table :tastes do |t|
      t.string :taste
      t.references :perception, index: true

      t.timestamps
    end
  end
end
