class CreateTerpenoids < ActiveRecord::Migration
  def change
    create_table :terpenoids do |t|
      t.references :LabTest, index: true
      t.float :concentration
      t.string :CasRegistryNumber
      t.string :MolecularFormula
      t.float :MolarMass
      t.float :ExactMolecularWeight
      t.float :PricePerG
      t.float :PricePerMole
      t.text :MedicinalProperties
      t.text :LiteratureReferences
      t.text :sources
      t.string :aroma
      t.string :hazards

      t.timestamps
    end
  end
end
