Rails.application.routes.draw do
  root 'home#index'
  namespace :api, defaults: {format: :json} do
    resources :strains do
      resource :genetic
      resource :growth_condition
      resource :medical
      resource :perception
      resource :subjective
      resources :terpenoids
      resources :cannabinoids
      resources :lab_tests do
        resources :lab_terps
        resources :lab_cans
      end
    end
      resource :selections do
        resources :medical_effects
        resources :medical_ailments
        resources :tastes
        resources :smells
        resources :appearances
        resources :geographic_origins
        resources :storage_container_types
        resources :drying_durations
        resources :curing_durations
        resources :terpenoids
        resources :cannabinoids
    end
   end
end
