class LabTerpSerializer < ActiveModel::Serializer
  attributes :id, :terpenoid_id, :concentration
  has_one :terpenoid
end
