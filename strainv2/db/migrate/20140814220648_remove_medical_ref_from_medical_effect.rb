class RemoveMedicalRefFromMedicalEffect < ActiveRecord::Migration
  def change
    remove_reference :medical_effects, :medical, index: true
  end
end
