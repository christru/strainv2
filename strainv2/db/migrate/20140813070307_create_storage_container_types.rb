class CreateStorageContainerTypes < ActiveRecord::Migration
  def change
    create_table :storage_container_types do |t|
      t.string :ContainerType
      t.references :GrowthCondition, index: true

      t.timestamps
    end
  end
end
