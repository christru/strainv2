class Api::DryingDurationsController < ApplicationController
  before_action :set_strain, only: [:show, :edit, :update, :destroy]
  respond_to :json

  def index
    respond_with DryingDuration.all
  end


end
