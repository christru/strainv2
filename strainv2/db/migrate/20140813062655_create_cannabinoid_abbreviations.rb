class CreateCannabinoidAbbreviations < ActiveRecord::Migration
  def change
    create_table :cannabinoid_abbreviations do |t|
      t.string :abbreviation
      t.references :cannabinoid, index: true

      t.timestamps
    end
  end
end
