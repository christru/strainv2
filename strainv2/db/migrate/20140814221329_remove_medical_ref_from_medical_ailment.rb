class RemoveMedicalRefFromMedicalAilment < ActiveRecord::Migration
  def change
    remove_reference :medical_ailments, :medical, index: true
  end
end
