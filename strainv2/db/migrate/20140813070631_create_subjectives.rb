class CreateSubjectives < ActiveRecord::Migration
  def change
    create_table :subjectives do |t|
      t.references :strain, index: true
      t.string :HowHigh
      t.text :news
      t.text :controversy
      t.text :TheScoop
      t.string :available
      t.string :concentrate
      t.string :flower
      t.text :TheVerdict
      t.string :lined
      t.text :pairings
      t.text :reviews
      t.text :PopCulture

      t.timestamps
    end
  end
end
