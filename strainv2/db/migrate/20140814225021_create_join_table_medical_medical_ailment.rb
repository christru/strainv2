class CreateJoinTableMedicalMedicalAilment < ActiveRecord::Migration
  def change
    create_join_table :medicals, :MedicalAilments do |t|
      # t.index [:medical_id, :medical_ailment_id]
      # t.index [:medical_ailment_id, :medical_id]
    end
  end
end
