class CreateDryingDurations < ActiveRecord::Migration
  def change
    create_table :drying_durations do |t|
      t.integer :duration
      t.references :GrowthCondition, index: true

      t.timestamps
    end
  end
end
