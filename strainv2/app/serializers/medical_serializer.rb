class MedicalSerializer < ActiveModel::Serializer
  attributes :id
  has_many :MedicalEffects, key: :MedicalEffect_ids
  has_many :MedicalAilments, key: :MedicalAilment_ids
end
