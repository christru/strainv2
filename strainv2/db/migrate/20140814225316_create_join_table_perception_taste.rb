class CreateJoinTablePerceptionTaste < ActiveRecord::Migration
  def change
    create_join_table :perceptions, :tastes do |t|
      # t.index [:perception_id, :taste_id]
      # t.index [:taste_id, :perception_id]
    end
  end
end
