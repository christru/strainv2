class RemoveLabTestRefFromCannabinoid < ActiveRecord::Migration
  def change
    remove_reference :cannabinoids, :LabTest, index: true
  end
end
