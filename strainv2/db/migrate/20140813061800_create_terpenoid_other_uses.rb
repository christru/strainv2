class CreateTerpenoidOtherUses < ActiveRecord::Migration
  def change
    create_table :terpenoid_other_uses do |t|
      t.string :use
      t.references :terpenoid, index: true

      t.timestamps
    end
  end
end
