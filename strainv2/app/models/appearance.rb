class Appearance < ActiveRecord::Base
  has_and_belongs_to_many :perceptions
end
