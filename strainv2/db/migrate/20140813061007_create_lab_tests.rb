class CreateLabTests < ActiveRecord::Migration
  def change
    create_table :lab_tests do |t|
      t.string :TestedBy
      t.date :DateTested
      t.references :strain, index: true

      t.timestamps
    end
  end
end
