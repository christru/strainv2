class CreateCannabinoidOtherUses < ActiveRecord::Migration
  def change
    create_table :cannabinoid_other_uses do |t|
      t.string :use
      t.references :cannabinoid, index: true

      t.timestamps
    end
  end
end
