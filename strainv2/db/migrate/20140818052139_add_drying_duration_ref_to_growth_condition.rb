class AddDryingDurationRefToGrowthCondition < ActiveRecord::Migration
  def change
    add_reference :growth_conditions, :DryingDuration, index: true
  end
end
