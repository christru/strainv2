class CreateGrowthConditions < ActiveRecord::Migration
  def change
    create_table :growth_conditions do |t|
      t.references :strain, index: true
      t.float :LightIntensity
      t.string :BulbType
      t.integer :BulbPower
      t.string :GrowingMedium
      t.float :VegetativeLight
      t.float :VegetativeDuration
      t.text :NutrientCompositionVeg
      t.float :FlowerLight
      t.float :FlowerDuration
      t.text :NutrientCompositionFlower
      t.float :DryingTemperature
      t.float :DryingRelativeHumidity
      t.float :StorageTemperature
      t.float :StorageDuration
      t.string :breeder
      t.integer :yield

      t.timestamps
    end
  end
end
