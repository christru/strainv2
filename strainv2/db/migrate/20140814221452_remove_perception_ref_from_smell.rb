class RemovePerceptionRefFromSmell < ActiveRecord::Migration
  def change
    remove_reference :smells, :perception, index: true
  end
end
