# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140818195921) do

  create_table "appearances", force: true do |t|
    t.string   "appearance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appearances_perceptions", id: false, force: true do |t|
    t.integer "perception_id", null: false
    t.integer "appearance_id", null: false
  end

  create_table "cannabinoid_abbreviations", force: true do |t|
    t.string   "abbreviation"
    t.integer  "cannabinoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cannabinoid_abbreviations", ["cannabinoid_id"], name: "index_cannabinoid_abbreviations_on_cannabinoid_id"

  create_table "cannabinoid_names", force: true do |t|
    t.string   "name"
    t.integer  "cannabinoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cannabinoid_names", ["cannabinoid_id"], name: "index_cannabinoid_names_on_cannabinoid_id"

  create_table "cannabinoid_other_uses", force: true do |t|
    t.string   "use"
    t.integer  "cannabinoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cannabinoid_other_uses", ["cannabinoid_id"], name: "index_cannabinoid_other_uses_on_cannabinoid_id"

  create_table "cannabinoid_synergies", force: true do |t|
    t.string   "synergy"
    t.integer  "cannabinoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cannabinoid_synergies", ["cannabinoid_id"], name: "index_cannabinoid_synergies_on_cannabinoid_id"

  create_table "cannabinoids", force: true do |t|
    t.float    "concentration"
    t.string   "CasRegistryNumber"
    t.string   "MolecularFormula"
    t.float    "MolarMass"
    t.float    "ExactMolecularWeight"
    t.float    "PricePerG"
    t.float    "PricePerMole"
    t.text     "MedicinalProperties"
    t.text     "LiteratureReferences"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  create_table "curing_durations", force: true do |t|
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "drying_durations", force: true do |t|
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genetics", force: true do |t|
    t.decimal  "SativaPercentage"
    t.decimal  "IndicaPercentage"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "genetics", ["strain_id"], name: "index_genetics_on_strain_id"

  create_table "geographic_origins", force: true do |t|
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "growth_conditions", force: true do |t|
    t.integer  "strain_id"
    t.float    "LightIntensity"
    t.string   "BulbType"
    t.integer  "BulbPower"
    t.string   "GrowingMedium"
    t.float    "VegetativeLight"
    t.float    "VegetativeDuration"
    t.text     "NutrientCompositionVeg"
    t.float    "FlowerLight"
    t.float    "FlowerDuration"
    t.text     "NutrientCompositionFlower"
    t.float    "DryingTemperature"
    t.float    "DryingRelativeHumidity"
    t.float    "StorageTemperature"
    t.float    "StorageDuration"
    t.string   "breeder"
    t.integer  "yield"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "StorageContainerType_id"
    t.integer  "CuringDuration_id"
    t.integer  "DryingDuration_id"
  end

  add_index "growth_conditions", ["CuringDuration_id"], name: "index_growth_conditions_on_CuringDuration_id"
  add_index "growth_conditions", ["DryingDuration_id"], name: "index_growth_conditions_on_DryingDuration_id"
  add_index "growth_conditions", ["StorageContainerType_id"], name: "index_growth_conditions_on_StorageContainerType_id"
  add_index "growth_conditions", ["strain_id"], name: "index_growth_conditions_on_strain_id"

  create_table "lab_cans", force: true do |t|
    t.integer  "lab_test_id"
    t.integer  "cannabinoid_id"
    t.float    "concentration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lab_terps", force: true do |t|
    t.integer  "terpenoid_id"
    t.float    "concentration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lab_test_id"
  end

  create_table "lab_tests", force: true do |t|
    t.string   "TestedBy"
    t.date     "DateTested"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lab_tests", ["strain_id"], name: "index_lab_tests_on_strain_id"

  create_table "labs", force: true do |t|
    t.boolean  "tested"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "labs", ["strain_id"], name: "index_labs_on_strain_id"

  create_table "medical_ailments", force: true do |t|
    t.string   "ailment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "medical_ailments_medicals", id: false, force: true do |t|
    t.integer "medical_id",        null: false
    t.integer "MedicalAilment_id", null: false
  end

  create_table "medical_effects", force: true do |t|
    t.string   "effect"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "medical_effects_medicals", id: false, force: true do |t|
    t.integer "medical_id",       null: false
    t.integer "MedicalEffect_id", null: false
  end

  create_table "medicals", force: true do |t|
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "medicals", ["strain_id"], name: "index_medicals_on_strain_id"

  create_table "perception_tastes", force: true do |t|
    t.integer  "perception_id"
    t.integer  "taste_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "perceptions", force: true do |t|
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "perceptions", ["strain_id"], name: "index_perceptions_on_strain_id"

  create_table "perceptions_smells", id: false, force: true do |t|
    t.integer "perception_id", null: false
    t.integer "smell_id",      null: false
  end

  create_table "smells", force: true do |t|
    t.string   "smell"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "storage_container_types", force: true do |t|
    t.string   "ContainerType"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "strain_akas", force: true do |t|
    t.string   "name"
    t.integer  "strain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "strain_akas", ["strain_id"], name: "index_strain_akas_on_strain_id"

  create_table "strains", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "GeographicOrigin"
    t.integer  "parent_id"
  end

  add_index "strains", ["parent_id"], name: "index_strains_on_parent_id"

  create_table "subjectives", force: true do |t|
    t.integer  "strain_id"
    t.string   "HowHigh"
    t.text     "news"
    t.text     "controversy"
    t.text     "TheScoop"
    t.string   "available"
    t.string   "concentrate"
    t.string   "flower"
    t.text     "TheVerdict"
    t.string   "lined"
    t.text     "pairings"
    t.text     "reviews"
    t.text     "PopCulture"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subjectives", ["strain_id"], name: "index_subjectives_on_strain_id"

  create_table "tastes", force: true do |t|
    t.string   "taste"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "terpenoid_akas", force: true do |t|
    t.string   "aka"
    t.integer  "terpenoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terpenoid_akas", ["terpenoid_id"], name: "index_terpenoid_akas_on_terpenoid_id"

  create_table "terpenoid_names", force: true do |t|
    t.string   "name"
    t.integer  "terpenoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terpenoid_names", ["terpenoid_id"], name: "index_terpenoid_names_on_terpenoid_id"

  create_table "terpenoid_other_uses", force: true do |t|
    t.string   "use"
    t.integer  "terpenoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terpenoid_other_uses", ["terpenoid_id"], name: "index_terpenoid_other_uses_on_terpenoid_id"

  create_table "terpenoid_synergies", force: true do |t|
    t.string   "synergy"
    t.integer  "terpenoid_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terpenoid_synergies", ["terpenoid_id"], name: "index_terpenoid_synergies_on_terpenoid_id"

  create_table "terpenoids", force: true do |t|
    t.float    "concentration"
    t.string   "CasRegistryNumber"
    t.string   "MolecularFormula"
    t.float    "MolarMass"
    t.float    "ExactMolecularWeight"
    t.float    "PricePerG"
    t.float    "PricePerMole"
    t.text     "MedicinalProperties"
    t.text     "LiteratureReferences"
    t.text     "sources"
    t.string   "aroma"
    t.string   "hazards"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

end
