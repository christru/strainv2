class Cannabinoid < ActiveRecord::Base
  has_many :LabCans
  has_many :LabTests, :through => :LabCans
end
