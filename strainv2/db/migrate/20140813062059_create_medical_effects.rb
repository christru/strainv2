class CreateMedicalEffects < ActiveRecord::Migration
  def change
    create_table :medical_effects do |t|
      t.string :effect
      t.references :medical, index: true

      t.timestamps
    end
  end
end
