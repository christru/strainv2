class MedicalEffectSerializer < ActiveModel::Serializer
  attributes :id, :effect
end
