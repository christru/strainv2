class Medical < ActiveRecord::Base
  belongs_to :strain
  has_and_belongs_to_many :MedicalEffects
  has_and_belongs_to_many :MedicalAilments
end
